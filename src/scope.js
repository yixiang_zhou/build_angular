'use strict';

function Scope() {
    this.$$watchers = [];
    this.$$lastDirtyWatcher = null;
    this.$$asyncQueue = [];
    this.$$applyAsyncQueue = [];
    this.$$phase = null;
    return this;
}

function initWatchVal() {}

Scope.prototype.$watch = function (watchFn, listenerFn, valueEq) {
    var watcher = {
        watchFn: watchFn,
        listenerFn: listenerFn || function () {},
        valueEq: !!valueEq,
        last: initWatchVal
    };

    this.$$watchers.push(watcher);
    this.$$lastDirtyWatcher = null;
};

Scope.prototype.$$areEqual = function (one, tow, valueEq) {
    if (valueEq) {
        return _.isEqual(one, tow);
    } else {
        return one === tow ||
            (typeof one === 'number' && typeof tow === 'number' && isNaN(one) && isNaN(tow));
    }
}

Scope.prototype.$digest = function () {
    var self = this;
    var hasDirtyWatcher;
    var iteration = 0;
    self.$$beginPhase('$digest');
    self.$$lastDirtyWatcher = null;
    do {
        hasDirtyWatcher = false;
        iteration ++;
        if (iteration > 10) {
            self.$$clearPhase();
            throw 'digest iteration exceeds 10 times';
        }

        while(self.$$asyncQueue.length) {
            var asyncTask = this.$$asyncQueue.shift();
            asyncTask.scope.$eval(asyncTask.expression);
        }

        _.forEach(this.$$watchers, function (watcher) {
            var newValue = watcher.watchFn(self);
            var oldValue = watcher.last;
            if (!self.$$areEqual(oldValue, newValue, watcher.valueEq)) {
                watcher.last = watcher.valueEq ? _.cloneDeep(newValue) : newValue;
                self.$$lastDirtyWatcher = watcher;
                watcher.listenerFn(newValue, oldValue === initWatchVal ? newValue : oldValue, self);
                hasDirtyWatcher = true;
            } else {
                if (self.$$lastDirtyWatcher === watcher) {
                    return false;
                }
            }
        });
    } while (hasDirtyWatcher || this.$$asyncQueue.length);
    self.$$clearPhase();
};

Scope.prototype.$eval = function (fn, args) {
    return fn(this, args);
};

Scope.prototype.$apply = function (fn) {
    try {
        this.$$beginPhase('$apply');
        return this.$eval(fn);
    } finally {
        this.$$clearPhase();
        this.$digest();
    }
};

Scope.prototype.$applyAsync = function (expr) {
    var self = this;
    self.$$applyAsyncQueue.push(function () {
        self.$eval(expr);
    });
    setTimeout(function () {
        self.$apply(function () {
            while(self.$$applyAsyncQueue.length) {
                self.$$applyAsyncQueue.shift()();
            }
        });
    }, 0);
};

Scope.prototype.$evalAsync = function(expr) {
    var self = this;
    if (!self.$$phase && !self.$$asyncQueue.length) {
        setTimeout(function () {
            if (self.$$asyncQueue.length) {
                self.$digest();
            }
        }, 0);
    }
    this.$$asyncQueue.push({scope: this, expression:expr});
};

Scope.prototype.$$beginPhase = function (phase) {
    if (this.$$phase === phase) {
        throw this.$$phase + ' already in progress';
    }
    this.$$phase = phase;
}

Scope.prototype.$$clearPhase = function () {
    this.$$phase = null;
}
